<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Relationship;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Relationship controller.
 *
 * @Route("relationship")
 */
class RelationshipController extends Controller
{
    /**
     * Lists all relationship entities.
     *
     * @Route("/", name="relationship_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $relationships = $em->getRepository('AppBundle:Relationship')->findAll();

        return $this->render('relationship/index.html.twig', array(
            'relationships' => $relationships,
        ));
    }

    /**
     * Creates a new relationship entity.
     *
     * @Route("/new", name="relationship_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $relationship = new Relationship();
        $form = $this->createForm('AppBundle\Form\RelationshipType', $relationship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($relationship);
            $em->flush($relationship);

            return $this->redirectToRoute('relationship_show', array('id' => $relationship->getId()));
        }

        return $this->render('relationship/new.html.twig', array(
            'relationship' => $relationship,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a relationship entity.
     *
     * @Route("/{id}", name="relationship_show")
     * @Method("GET")
     */
    public function showAction(Relationship $relationship)
    {
        $deleteForm = $this->createDeleteForm($relationship);

        return $this->render('relationship/show.html.twig', array(
            'relationship' => $relationship,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing relationship entity.
     *
     * @Route("/{id}/edit", name="relationship_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Relationship $relationship)
    {
        $deleteForm = $this->createDeleteForm($relationship);
        $editForm = $this->createForm('AppBundle\Form\RelationshipType', $relationship);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('relationship_edit', array('id' => $relationship->getId()));
        }

        return $this->render('relationship/edit.html.twig', array(
            'relationship' => $relationship,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a relationship entity.
     *
     * @Route("/{id}", name="relationship_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Relationship $relationship)
    {
        $form = $this->createDeleteForm($relationship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($relationship);
            $em->flush($relationship);
        }

        return $this->redirectToRoute('relationship_index');
    }

    /**
     * Creates a form to delete a relationship entity.
     *
     * @param Relationship $relationship The relationship entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Relationship $relationship)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('relationship_delete', array('id' => $relationship->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Decline relationship request from another user.
     *
     * @Route("/getcontactslist", name="getcontactslist")
     * @Method({"GET", "POST"})
     */
    public function getContactsListAction(Request $request) {
        $data = null;
        $contacts = array();
        $relationshipInitiators = array();
        $relationshipCompromises = array();
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $relationshipInitiators = $em->getRepository('AppBundle:Relationship')->createQueryBuilder('r')
            ->where('r.compromise = :compromise')
            ->andWhere('r.initiator != :initiator')
            ->setParameter('compromise', $data['currentUserId'])
            ->setParameter('initiator', $data['currentUserId'])
            ->getQuery()->getResult();

        $relationshipCompromises = $em->getRepository('AppBundle:Relationship')->createQueryBuilder('r')
            ->where('r.initiator = :initiator')
            ->andWhere('r.compromise != :compromise')
            ->setParameter('compromise', $data['currentUserId'])
            ->setParameter('initiator', $data['currentUserId'])
            ->getQuery()->getResult();

        foreach ($relationshipInitiators as $rInitiator) {
            $contacts[] = array(
                'id' => $rInitiator->getInitiator()->getId(),
                'firstName' => $rInitiator->getInitiator()->getFirstName(),
                'secondName' => $rInitiator->getInitiator()->getSecondName(),
                'lastName' => $rInitiator->getInitiator()->getLastName(),
                'username' => $rInitiator->getInitiator()->getUsername(),
                'email' => $rInitiator->getInitiator()->getEmail(),
                'addInfo' => array(
                    'relationshipId' => $rInitiator->getId(),
                    'initiatorId' => $rInitiator->getInitiator()->getId(),
                    'compromiseId' => $rInitiator->getCompromise()->getId(),
                    'relationshipRequestId' => $rInitiator->getRelationshipRequest()->getId()
                )
            );
        }

        foreach ($relationshipCompromises as $rCompromise) {
            $contacts[] = array(
                'id' => $rCompromise->getCompromise()->getId(),
                'firstName' => $rCompromise->getCompromise()->getFirstName(),
                'secondName' => $rCompromise->getCompromise()->getSecondName(),
                'lastName' => $rCompromise->getCompromise()->getLastName(),
                'username' => $rCompromise->getCompromise()->getUsername(),
                'email' => $rCompromise->getCompromise()->getEmail(),
                'addInfo' => array(
                    'relationshipId' => $rCompromise->getId(),
                    'initiatorId' => $rCompromise->getInitiator()->getId(),
                    'compromiseId' => $rCompromise->getCompromise()->getId(),
                    'relationshipRequestId' => $rCompromise->getRelationshipRequest()->getId()
                )
            );
        }

        return new JsonResponse(array('contacts' => $contacts));
    }

    /**
     * Remove user from user's contact list.
     *
     * @Route("/removeuserfromcontactlist", name="removeuserfromcontactlist")
     * @Method({"GET", "POST"})
     */
    public function removeUserFromContactList(Request $request) {
        $data = null;
        $relationship = null;
        $relationshipRequest = null;
        $currentUserId = null;
        $targetUserId = null;
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $chatMessages = array();

        $removeUserFromContactListStatus = array(
            'error' => false
        );

        $currentUserId = $data['currentUserId'];
        $targetUserId = $data['targetUserId'];

        $relationship = $em->getRepository('AppBundle:Relationship')->createQueryBuilder('r')
            ->where('r.id = :id')
            ->andWhere('r.relationshipRequest = :relationshipRequest')
            ->setParameter('id', $data['relationshipId'])
            ->setParameter('relationshipRequest', $data['relationshipRequestId'])
            ->getQuery()->getOneOrNullResult();

        $relationshipRequest = $em->getRepository('AppBundle:RelationshipRequest')->createQueryBuilder('rr')
            ->where('rr.id = :id')
            ->setParameter('id', $data['relationshipRequestId'])
            ->getQuery()->getOneOrNullResult();

        $chatMessages = $em->getRepository('AppBundle:Chat')->createQueryBuilder('c')
            ->where('(c.fromUser = :currentUser AND c.toUser = :targetUser) OR (c.fromUser = :targetUser AND c.toUser = :currentUser)')
            ->setParameter('currentUser', $currentUserId)
            ->setParameter('targetUser', $targetUserId)
            ->getQuery()->getResult();

        if ($relationship && $relationshipRequest) {
            $em->remove($relationship);
            $em->remove($relationshipRequest);
        } else {
            $removeUserFromContactListStatus['error'] = true;
        }

        if ($chatMessages && count($chatMessages) > 0) {
            foreach ($chatMessages as $chatMessage) {
                $em->remove($chatMessage);
            }
        }

        $em->flush();

        return new JsonResponse(array('status' => $removeUserFromContactListStatus));
    }
}
