<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Relationship;
use AppBundle\Entity\RelationshipRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Relationshiprequest controller.
 *
 * @Route("relationshiprequest")
 */
class RelationshipRequestController extends Controller
{
    /**
     * Lists all relationshipRequest entities.
     *
     * @Route("/", name="relationshiprequest_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $relationshipRequests = $em->getRepository('AppBundle:RelationshipRequest')->findAll();

        return $this->render('relationshiprequest/index.html.twig', array(
            'relationshipRequests' => $relationshipRequests,
        ));
    }

    /**
     * Creates a new relationshipRequest entity.
     *
     * @Route("/new", name="relationshiprequest_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $relationshipRequest = new Relationshiprequest();
        $form = $this->createForm('AppBundle\Form\RelationshipRequestType', $relationshipRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($relationshipRequest);
            $em->flush($relationshipRequest);

            return $this->redirectToRoute('relationshiprequest_show', array('id' => $relationshipRequest->getId()));
        }

        return $this->render('relationshiprequest/new.html.twig', array(
            'relationshipRequest' => $relationshipRequest,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a relationshipRequest entity.
     *
     * @Route("/{id}", name="relationshiprequest_show")
     * @Method("GET")
     */
    public function showAction(RelationshipRequest $relationshipRequest)
    {
        $deleteForm = $this->createDeleteForm($relationshipRequest);

        return $this->render('relationshiprequest/show.html.twig', array(
            'relationshipRequest' => $relationshipRequest,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing relationshipRequest entity.
     *
     * @Route("/{id}/edit", name="relationshiprequest_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, RelationshipRequest $relationshipRequest)
    {
        $deleteForm = $this->createDeleteForm($relationshipRequest);
        $editForm = $this->createForm('AppBundle\Form\RelationshipRequestType', $relationshipRequest);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('relationshiprequest_edit', array('id' => $relationshipRequest->getId()));
        }

        return $this->render('relationshiprequest/edit.html.twig', array(
            'relationshipRequest' => $relationshipRequest,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a relationshipRequest entity.
     *
     * @Route("/{id}", name="relationshiprequest_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, RelationshipRequest $relationshipRequest)
    {
        $form = $this->createDeleteForm($relationshipRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($relationshipRequest);
            $em->flush($relationshipRequest);
        }

        return $this->redirectToRoute('relationshiprequest_index');
    }

    /**
     * Creates a form to delete a relationshipRequest entity.
     *
     * @param RelationshipRequest $relationshipRequest The relationshipRequest entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(RelationshipRequest $relationshipRequest)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('relationshiprequest_delete', array('id' => $relationshipRequest->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Add request with status "new" to add user to my list of users.
     *
     * @Route("/addusertomylist", name="addusertomylist")
     * @Method({"GET", "POST"})
     */
    public function addUserToMyListAction(Request $request) {
        $addUserToMyListStatus = array(
            'error' => false,
            'isAdded' => true
        );

        $data = $request->getContent();
        $fromTo = json_decode($data, true);

        $em = $this->getDoctrine()->getManager();
        $fromUser = $em->getRepository('AppBundle:User')->find($fromTo['from']);
        $toUser = $em->getRepository('AppBundle:User')->find($fromTo['to']);

        $relationshipRequest = new RelationshipRequest();

        $relationshipRequest->setCreatedAt(new \DateTime('now'));
        $relationshipRequest->setUpdatedAt(new \DateTime('now'));
        if ($fromUser) {
            $relationshipRequest->setFrom($fromUser);
        } else {
            $addUserToMyListStatus['isAdded'] = false;
            $addUserToMyListStatus['error'] = true;
            return new JsonResponse(array('status' => $addUserToMyListStatus));
        }
        if ($toUser) {
            $relationshipRequest->setTo($toUser);
        } else {
            $addUserToMyListStatus['isAdded'] = false;
            $addUserToMyListStatus['error'] = true;
            return new JsonResponse(array('status' => $addUserToMyListStatus));
        }
        if ($fromUser && $toUser) {
            $relationshipRequest->setStatus('new');

            $em->persist($relationshipRequest);
            $em->flush();
        }

        return new JsonResponse(array('status' => $addUserToMyListStatus));
    }

    /**
     * Get relationship requests for current user with status new.
     *
     * @Route("/relationshiprequestsstatusnew", name="relationshiprequestsstatusnew")
     * @Method({"GET", "POST"})
     */
    public function getRelationshipRequestsStatusNewAction(Request $request) {
        $data = $request->getContent();
        $currentUserData = json_decode($data, true);
        $em = $this->getDoctrine()->getManager();
        $relationshipRequests = array();
        $preparedRR = array();

        $relationshipRequests = $em->getRepository('AppBundle:RelationshipRequest')
            ->createQueryBuilder('rr')
            ->where('rr.to = :toUser')
            ->andWhere('rr.status = :status')
            ->setParameter('toUser', $currentUserData['currentUserId'])
            ->setParameter('status', 'new')
            ->getQuery()->getResult();

        if ($relationshipRequests && count($relationshipRequests) > 0) {
            foreach ($relationshipRequests as $rRequest) {
                $tmpRRequest['id'] = $rRequest->getId();
                $tmpRRequest['fromId'] = $rRequest->getFrom()->getId();
                $tmpRRequest['fromFirstName'] = $rRequest->getFrom()->getFirstName();
                $tmpRRequest['fromSecondName'] = $rRequest->getFrom()->getSecondName();
                $tmpRRequest['fromLastName'] = $rRequest->getFrom()->getLastName();
                $tmpRRequest['fromEmailName'] = $rRequest->getFrom()->getEmail();
                $tmpRRequest['fromUsername'] = $rRequest->getFrom()->getUsername();

                $preparedRR[] = $tmpRRequest;
            }
        }

        return new JsonResponse(array('preparedRR' => $preparedRR));
    }

    /**
     * Accepted relationship request from another user.
     *
     * @Route("/acceptrelationshiprequest", name="acceptrelationshiprequest")
     * @Method({"GET", "POST"})
     */
    public function acceptRelationshipRequestAction(Request $request) {
        $acceptRelationshipRequestData = null;
        $data = null;
        $initiator = null;
        $compromise = null;
        $rRequest = null;
        $relationship = null;
        $preparedRR = array();
        $acceptRelationshipRequestStatus = array(
            'error' => false,
            'isAccept' => true
        );

        $data = $request->getContent();
        $acceptRelationshipRequestData = json_decode($data, true);
        $em = $this->getDoctrine()->getManager();

        $initiator = $em->getRepository('AppBundle:User')->find($acceptRelationshipRequestData['from']);
        $compromise = $em->getRepository('AppBundle:User')->find($acceptRelationshipRequestData['currentUserId']);
        $rRequest = $em->getRepository('AppBundle:RelationshipRequest')->find($acceptRelationshipRequestData['relationshipRequest']);

        $relationship = new Relationship();
        $relationship->setInitiator($initiator);
        $relationship->setCompromise($compromise);
        $relationship->setRelationshipRequest($rRequest);
        $relationship->setUpdatedAt(new \DateTime('now'));
        $relationship->setCreatedAt(new \DateTime('now'));

        $rRequest->setStatus('accepted');
        $rRequest->setUpdatedAt(new \DateTime('now'));

        $em->persist($relationship);
        $em->persist($rRequest);
        $em->flush();

        $relationshipRequests = $em->getRepository('AppBundle:RelationshipRequest')
            ->createQueryBuilder('rr')
            ->where('rr.to = :toUser')
            ->andWhere('rr.status = :status')
            ->setParameter('toUser', $acceptRelationshipRequestData['currentUserId'])
            ->setParameter('status', 'new')
            ->getQuery()->getResult();

        if ($relationshipRequests && count($relationshipRequests) > 0) {
            foreach ($relationshipRequests as $rRequest) {
                $tmpRRequest['id'] = $rRequest->getId();
                $tmpRRequest['fromId'] = $rRequest->getFrom()->getId();
                $tmpRRequest['fromFirstName'] = $rRequest->getFrom()->getFirstName();
                $tmpRRequest['fromSecondName'] = $rRequest->getFrom()->getSecondName();
                $tmpRRequest['fromLastName'] = $rRequest->getFrom()->getLastName();
                $tmpRRequest['fromEmailName'] = $rRequest->getFrom()->getEmail();
                $tmpRRequest['fromUsername'] = $rRequest->getFrom()->getUsername();

                $preparedRR[] = $tmpRRequest;
            }
        }

        return new JsonResponse(array('preparedRR' => $preparedRR, 'status' => $acceptRelationshipRequestStatus));
    }

    /**
     * Decline relationship request from another user.
     *
     * @Route("/declinerelationshiprequest", name="declinerelationshiprequest")
     * @Method({"GET", "POST"})
     */
    public function declineRelationshipRequestAction(Request $request) {
        $declineRelationshipRequestData = null;
        $data = null;
        $fromUser = null;
        $toUser = null;
        $rRequest = null;
        $preparedRRequests = array();
        $declineRelationshipRequestStatus = array(
            'error' => false,
            'isExist' => false
        );

        $data = $request->getContent();
        $declineRelationshipRequestData = json_decode($data, true);
        $em = $this->getDoctrine()->getManager();

        $fromUser = $em->getRepository('AppBundle:User')->find($declineRelationshipRequestData['fromUserId']);
        $toUser = $em->getRepository('AppBundle:User')->find($declineRelationshipRequestData['toUserId']);
        $rRequest = $em->getRepository('AppBundle:RelationshipRequest')->createQueryBuilder('rr')
            ->where('rr.id = :id')
            ->andWhere('rr.from = :fromUser')
            ->andWhere('rr.to = :toUser')
            ->setParameter('id', $declineRelationshipRequestData['rRequestId'])
            ->setParameter('fromUser', $fromUser)
            ->setParameter('toUser', $toUser)
            ->getQuery()->getOneOrNullResult();

        if ($rRequest) {
            $declineRelationshipRequestStatus['isExist'] = true;

            $em->remove($rRequest);
            $em->flush();
        }

        $preparedRRequests = $this->getRelationshipRequestStatusNew($toUser);

        return new JsonResponse(array('preparedRR' => $preparedRRequests, 'status' => $declineRelationshipRequestStatus));
    }

    public function getRelationshipRequestStatusNew($toUserId) {
    $em = $this->getDoctrine()->getManager();
    $preparedRR = array();

    $relationshipRequests = $em->getRepository('AppBundle:RelationshipRequest')
        ->createQueryBuilder('rr')
        ->where('rr.to = :toUser')
        ->andWhere('rr.status = :status')
        ->setParameter('toUser', $toUserId)
        ->setParameter('status', 'new')
        ->getQuery()->getResult();

        if ($relationshipRequests && count($relationshipRequests) > 0) {
            foreach ($relationshipRequests as $rRequest) {
                $tmpRRequest['id'] = $rRequest->getId();
                $tmpRRequest['fromId'] = $rRequest->getFrom()->getId();
                $tmpRRequest['fromFirstName'] = $rRequest->getFrom()->getFirstName();
                $tmpRRequest['fromSecondName'] = $rRequest->getFrom()->getSecondName();
                $tmpRRequest['fromLastName'] = $rRequest->getFrom()->getLastName();
                $tmpRRequest['fromEmailName'] = $rRequest->getFrom()->getEmail();
                $tmpRRequest['fromUsername'] = $rRequest->getFrom()->getUsername();

                $preparedRR[] = $tmpRRequest;
            }
        }

        return $preparedRR;
    }
}
