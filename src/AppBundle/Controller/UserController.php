<?php

namespace AppBundle\Controller;

use AppBundle\Entity\RelationshipRequest;
use AppBundle\Entity\User;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $arrUser = array();
        $userModel = array();
        $user = null;
        $data = null;
        $email = '';
        $password = '';
        $userName = '';
        $avatarImage = null;

        $registerStatus = array(
            'isRegistered' => false,
            'isExistEmail' => false,
            'isExistUser' => false
        );
        $em = $this->getDoctrine()->getManager();
        $jmsSerializer = SerializerBuilder::create()->build();

        // Get avatarImage (File type)
        $avatarImage = $request->files->get('avatarImage');
        // Get registerModel data (Json string) and decode it
        $userModel = json_decode($request->get('registerModel'), true);
        $firstName = $userModel['firstName'];
        $secondName = $userModel['secondName'];
        $lastName = $userModel['lastName'];
        $email = $userModel['email'];
        $password = $userModel['password'];
        $userName = $userModel['nickName'];


        $user = new User();
        $user->setFirstName($firstName);
        $user->setSecondName($secondName);
        $user->setLastName($lastName);
        $user->setUsername($userName);
        $user->setPassword($password);
        $user->setPlainPassword($password);
        $user->setEmail($email);

        // Do pre persist and pre flush to get user "id" (after persist and flush the "id" is available now) for user's folder to save avatar image
        // or something else there.
        $em->persist($user);
        $em->flush();

        if ($avatarImage) {
            $user->saveAvatarImageFile($avatarImage);
        }

        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        $em->persist($user);
        $em->flush();

        if ($user && $user->getId()) {
            $registerStatus['isExistUser'] = true;
        }

        $arrUser = $jmsSerializer->toArray($user);

        return new JsonResponse(array('status' => $registerStatus, 'user' => $arrUser));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush($user);
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Login user into system by email or username and password.
     *
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request) {
        $loginStatus = array(
            'error' => false,
            'isExistUser' => false
        );

        $arrUser = array();
        $isValid = null;
        $jmsSerializer = SerializerBuilder::create()->build();
        $data = $request->getContent();

        $userModel = json_decode($data, true);
        $email = $userModel['email'];
        $password = $userModel['password'];

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->createQueryBuilder('u')
            ->where('u.email = :email')
            ->orWhere('u.username = :username')
            ->setParameter('email', $email)
            ->setParameter('username', $email)
            ->getQuery()->getOneOrNullResult();

        if ($user) {
            $isValid = $this->get('security.password_encoder')
                ->isPasswordValid($user, $password);
        } else {
            $loginStatus['isExistUser'] = false;
        }

        if ($user && $isValid) {
            $loginStatus['isExistUser'] = $isValid;
            $arrUser = $jmsSerializer->toArray($user);
        } else {
            $loginStatus['isExistUser'] = false;
        }

        return new JsonResponse(array('status' => $loginStatus, 'user' => $arrUser));
    }

    /**
     * Get user profile settings by id.
     *
     * @Route("/getuserprofile", name="getuserprofile")
     * @Method({"GET", "POST"})
     */
    public function getUserProfileAction(Request $request) {
        $userPofileStatus = array(
            'error' => false,
            'isExistUser' => false
        );

        $arrUser = array();
        $jmsSerializer = SerializerBuilder::create()->build();
        $data = $request->getContent();

        $id = json_decode($data, true);
        $id = $id['id'];

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if ($user) {
            $userPofileStatus['isExistUser'] = true;
            $arrUser = $jmsSerializer->toArray($user);
        } else {
            $userPofileStatus['isExistUser'] = false;
        }

        return new JsonResponse(array('status' => $userPofileStatus, 'user' => $arrUser));
    }

    /**
     * update user profile settings by id.
     *
     * @Route("/updateuserprofile", name="updateuserprofile")
     * @Method({"GET", "POST"})
     */
    public function updateUserProfileAction(Request $request) {
        $userPofileStatus = array(
            'error' => false,
            'isExistUser' => false
        );

        $arrUser = array();
        $jmsSerializer = SerializerBuilder::create()->build();
        $data = $request->getContent();

        $userProfileSetting = json_decode($data, true);

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($userProfileSetting['id']);

        if ($user) {
            $userPofileStatus['isExistUser'] = true;
            $user->setFirstName($userProfileSetting['firstName']);
            $user->setSecondName($userProfileSetting['secondName']);
            $user->setLastName($userProfileSetting['lastName']);
            $user->setEmail($userProfileSetting['email']);
            $user->setUsername($userProfileSetting['username']);

            if (!empty($userProfileSetting['password']) && !empty($userProfileSetting['passwordConfirmation']) && $userProfileSetting['password'] == $userProfileSetting['passwordConfirmation']) {
                $user->setPlainPassword($userProfileSetting['password']);
            }
            $em->persist($user);
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updatePassword($user);
            $em->flush();

            $arrUser = $jmsSerializer->toArray($user);
        } else {
            $userPofileStatus['isExistUser'] = false;
        }

        return new JsonResponse(array('status' => $userPofileStatus, 'user' => $arrUser));
    }

    /**
     * Search users by searching string. Get user relationship requests and separate by statuses.
     *
     * @Route("/searchuser", name="searchuser")
     * @Method({"GET", "POST"})
     */
    public function searchUsersAction(Request $request) {
        $userSearchStatus = array(
            'error' => false,
            'isExistUser' => false
        );
        $preparedUsers = array();
        $users = array();
        $relationshipRequests = array();
        $preparedRelationshipRequests = array();
        $rRequestStatusNew = array();
        $rRequestStatusDeclined = array();
        $rRequestStatusAccepted = array();

        $jmsSerializer = SerializerBuilder::create()->build();
        $data = $request->getContent();
        $searchingString = json_decode($data, true);

        $em = $this->getDoctrine()->getManager();
        if (!empty($searchingString['searchStr']) && strlen($searchingString['searchStr']) >= 2) {
        $users = $em->getRepository('AppBundle:User')
            ->createQueryBuilder('u')
            ->where('u.firstName LIKE :firstName')
            ->orWhere('u.secondName LIKE :secondName')
            ->orWhere('u.lastName LIKE :lastName')
            ->orWhere('u.username LIKE :username')
            ->orWhere('u.email LIKE :email')
            ->andWhere('u.id != :user')
            ->setParameter('firstName', '%'.$searchingString['searchStr'].'%')
            ->setParameter('secondName', '%'.$searchingString['searchStr'].'%')
            ->setParameter('lastName', '%'.$searchingString['searchStr'].'%')
            ->setParameter('username', '%'.$searchingString['searchStr'].'%')
            ->setParameter('email', '%'.$searchingString['searchStr'].'%')
            ->setParameter('user', $searchingString['currentUserId'])
            ->getQuery()->getResult();

        $relationshipRequests = $em->getRepository('AppBundle:RelationshipRequest')
            ->createQueryBuilder('rr')
            ->where('rr.from = :user')
            ->setParameter('user', $searchingString['currentUserId'])
            ->getQuery()->getResult();
        }

        if ($users && count($users) > 0) {
            foreach ($users as $user) {
                $tmpUser['id'] = $user->getId();
                $tmpUser['firstName'] = $user->getFirstName();
                $tmpUser['secondName'] = $user->getSecondName();
                $tmpUser['lastName'] = $user->getLastName();
                $tmpUser['username'] = $user->getUsername();
                $tmpUser['email'] = $user->getEmail();

                $preparedUsers[] = $tmpUser;
            }
        }

        if ($relationshipRequests && count($relationshipRequests) > 0) {
            foreach ($relationshipRequests as $relationshipRequest) {
                $preparedRelationshipRequests[] = array(
                    'from' => $relationshipRequest->getFrom()->getId(),
                    'to' => $relationshipRequest->getTo()->getId(),
                    'status' => $relationshipRequest->getStatus()
                );

                if ($relationshipRequest->getStatus() == 'new') {
                    $rRequestStatusNew[] = $relationshipRequest->getTo()->getId();
                }
                if ($relationshipRequest->getStatus() == 'accepted') {
                    $rRequestStatusAccepted[] = $relationshipRequest->getTo()->getId();
                }
                if ($relationshipRequest->getStatus() == 'declined') {
                    $rRequestStatusDeclined[] = $relationshipRequest->getTo()->getId();
                }
            }
        }

        return new JsonResponse(array(
            'status' => $userSearchStatus,
            'users' => $preparedUsers,
            'relationshipRequests' => $preparedRelationshipRequests,
            'rRequestStatusNew' => $rRequestStatusNew,
            'rRequestStatusAccepted' => $rRequestStatusAccepted,
            'rRequestStatusDeclined' => $rRequestStatusDeclined));
    }
}
