<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Chat;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Chat controller.
 *
 * @Route("chat")
 */
class ChatController extends Controller
{
    /**
     * Lists all chat entities.
     *
     * @Route("/", name="chat_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $chats = $em->getRepository('AppBundle:Chat')->findAll();

        return $this->render('chat/index.html.twig', array(
            'chats' => $chats,
        ));
    }

    /**
     * Creates a new chat entity.
     *
     * @Route("/new", name="chat_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $chat = new Chat();
        $form = $this->createForm('AppBundle\Form\ChatType', $chat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($chat);
            $em->flush();

            return $this->redirectToRoute('chat_show', array('id' => $chat->getId()));
        }

        return $this->render('chat/new.html.twig', array(
            'chat' => $chat,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a chat entity.
     *
     * @Route("/{id}", name="chat_show")
     * @Method("GET")
     */
    public function showAction(Chat $chat)
    {
        $deleteForm = $this->createDeleteForm($chat);

        return $this->render('chat/show.html.twig', array(
            'chat' => $chat,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing chat entity.
     *
     * @Route("/{id}/edit", name="chat_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Chat $chat)
    {
        $deleteForm = $this->createDeleteForm($chat);
        $editForm = $this->createForm('AppBundle\Form\ChatType', $chat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chat_edit', array('id' => $chat->getId()));
        }

        return $this->render('chat/edit.html.twig', array(
            'chat' => $chat,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a chat entity.
     *
     * @Route("/{id}", name="chat_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Chat $chat)
    {
        $form = $this->createDeleteForm($chat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($chat);
            $em->flush();
        }

        return $this->redirectToRoute('chat_index');
    }

    /**
     * Creates a form to delete a chat entity.
     *
     * @param Chat $chat The chat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Chat $chat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chat_delete', array('id' => $chat->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * SAve chat message.
     *
     * @Route("/keepcorrespondence", name="keepcorrespondence")
     * @Method({"GET", "POST"})
     */
    public function keepCorrespondence(Request $request)
    {
        $keepCorrespondence = array('error' => false);
        $data = null;
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $fromUser = null;
        $toUser = null;

        $fromUser = $em->getRepository('AppBundle:User')->find($data['chatMessage']['from']);
        $toUser = $em->getRepository('AppBundle:User')->find($data['chatMessage']['to']);

        $chat = new Chat();

        if ($fromUser && $toUser) {
            $chat->setFromUser($fromUser);
            $chat->setToUser($toUser);
        } else {
            $keepCorrespondence['error'] = true;
            return new JsonResponse(array('keepCorrespondence' => $keepCorrespondence));
        }

        $chat->setMessage($data['chatMessage']['message']);
        $chat->setTime(new \DateTime($data['chatMessage']['time']));

        $em->persist($chat);
        $em->flush();

        return new JsonResponse(array('keepCorrespondence' => $keepCorrespondence));
    }

    /**
     * Get chat messages.
     *
     * @Route("/getchatcorrespondence", name="getchatcorrespondence")
     * @Method({"GET", "POST"})
     */
    public function getChatCorrespondence(Request $request)
    {
        $correspondence = array();
        $preparedCorrespondence = array();
        $data = null;
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $correspondence = $em->getRepository('AppBundle:Chat')->createQueryBuilder('c')
            ->where('(c.fromUser = :fromUser AND c.toUser = :toUser) OR (c.toUser = :fromUser AND c.fromUser = :toUser)')
            ->setParameter('fromUser', $data['from'])
            ->setParameter('toUser', $data['to'])
            ->getQuery()->getResult();

        if ($correspondence && count($correspondence) > 0) {
            foreach ($correspondence as $item) {
                $preparedCorrespondence[] = array(
                    'from' => $item->getFromUser()->getId(),
                    'to' => $item->getToUser()->getId(),
                    'email' => $item->getFromUser()->getEmail(),
                    'firstName' => $item->getFromUser()->getFirstName(),
                    'secondName' => $item->getFromUser()->getSecondName(),
                    'message' => $item->getMessage(),
                    //'time' => $item->getTime()
                );
            }
        }

        return new JsonResponse(array('correspondence' => $preparedCorrespondence));
    }
}
