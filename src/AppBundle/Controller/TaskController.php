<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Task controller.
 *
 * @Route("task")
 */
class TaskController extends Controller
{
    /**
     * Lists all task entities.
     *
     * @Route("/", name="task_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tasks = $em->getRepository('AppBundle:Task')->findAll();

        return $this->render('task/index.html.twig', array(
            'tasks' => $tasks,
        ));
    }

    /**
     * Creates a new task entity and set own task user that created the task.
     *
     * @Route("/new", name="task_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $taskStatus = array('error' => false, 'message' => '');
        $em = $this->getDoctrine()->getManager();
        $jmsSerializer = SerializerBuilder::create()->build();
        $data = $request->getContent();
        $taskModel = json_decode($data, true);
        $fromUser = null;
        $toUser = null;

        $fromUser = $em->getRepository('AppBundle:User')->find($taskModel['from']);
        $toUser = $em->getRepository('AppBundle:User')->find($taskModel['to']);

        if (!$fromUser) {
            $taskStatus['error'] = true;
            $taskStatus['message'] = 'The sender of the task was not found.';
            return new JsonResponse(array('status' => $taskStatus));
        }

        if (!$toUser) {
            $taskStatus['error'] = true;
            $taskStatus['message'] = 'The recipient of the task was not found.';
            return new JsonResponse(array('status' => $taskStatus));
        }

        $task = new Task();
        $task->setTitle($taskModel['title']);
        $task->setDescription($taskModel['description']);
        $task->setPriority($taskModel['priority']);
        $task->setStatus($taskModel['status']);

        $task->setFromUser($fromUser);
        $task->setToUser($toUser);

        $em->persist($task);
        $em->flush();

        $arrTask = $jmsSerializer->toArray($task);

        return new JsonResponse(array('task' => $arrTask, 'status' => $taskStatus));
    }

    /**
     * Finds and displays a task entity.
     *
     * @Route("/{id}", name="task_show")
     * @Method("GET")
     */
    public function showAction(Task $task)
    {
        $deleteForm = $this->createDeleteForm($task);

        return $this->render('task/show.html.twig', array(
            'task' => $task,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing task entity.
     *
     * @Route("/{id}/edit", name="task_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Task $task)
    {
        $deleteForm = $this->createDeleteForm($task);
        $editForm = $this->createForm('AppBundle\Form\TaskType', $task);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('task_edit', array('id' => $task->getId()));
        }

        return $this->render('task/edit.html.twig', array(
            'task' => $task,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a task entity.
     *
     * @Route("/{id}", name="task_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Task $task)
    {
        $form = $this->createDeleteForm($task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($task);
            $em->flush($task);
        }

        return $this->redirectToRoute('task_index');
    }

    /**
     * Creates a form to delete a task entity.
     *
     * @param Task $task The task entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Task $task)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('task_delete', array('id' => $task->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
