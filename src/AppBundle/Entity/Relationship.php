<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relationship
 *
 * @ORM\Table(name="relationship")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RelationshipRepository")
 */
class Relationship
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $initiator;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $compromise;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\RelationshipRequest")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $relationshipRequest;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Relationship
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Relationship
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set initiator
     *
     * @param User $initiator
     *
     * @return Relationship
     */
    public function setInitiator(User $initiator)
    {
        $this->initiator = $initiator;

        return $this;
    }

    /**
     * Get initiator
     *
     * @return mixed
     */
    public function getInitiator()
    {
        return $this->initiator;
    }

    /**
     * Set compromise
     *
     * @param User $compromise
     *
     * @return Relationship
     */
    public function setCompromise(User $compromise)
    {
        $this->compromise = $compromise;

        return $this;
    }

    /**
     * Get compromise
     *
     * @return mixed
     */
    public function getCompromise()
    {
        return $this->compromise;
    }

    /**
     * Set relationshipRequest
     *
     * @param RelationshipRequest $relationshipRequest
     *
     * @return Relationship
     */
    public function setRelationshipRequest(RelationshipRequest $relationshipRequest)
    {
        $this->relationshipRequest = $relationshipRequest;

        return $this;
    }

    /**
     * Get relationshipRequest
     *
     * @return mixed
     */
    public function getRelationshipRequest()
    {
        return $this->relationshipRequest;
    }
}

