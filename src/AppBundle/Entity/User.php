<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

const USER_FOLDER = './users/';
const BASE_URL = 'http://artemstarchkov.on.kg/';

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="secondName", type="string", length=255, nullable=true)
     */
    private $secondName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="avatarImage", type="text", nullable=true)
     */
    private $avatarImage;

    /**
     * @var string
     *
     * @ORM\Column(name="linkAvatarImage", type="text", nullable=true)
     */
    private $linkAvatarImage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     *
     * @return User
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get secondName
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set avatarImage
     *
     * @param string $avatarImage
     *
     * @return User
     */
    public function setAvatarImage($avatarImage)
    {
        $this->avatarImage = $avatarImage;

        return $this;
    }

    /**
     * Get avatarImage
     *
     * @return string
     */
    public function getAvatarImage()
    {
        return $this->avatarImage;
    }

    public function saveAvatarImageFile(UploadedFile $file) {
        $userFolder = USER_FOLDER.'user_id_'.$this->id;

        if (file_exists($userFolder)) {
            $file->move($userFolder, $file->getClientOriginalName());
            $this->setAvatarImage($userFolder.'/'.$file->getClientOriginalName());
            $this->setLinkAvatarImage(BASE_URL.$this->getAvatarImage());
            return array('error' => false, 'message' => 'User\'s avatar file was successful uploaded.');
        } else {
            if (mkdir($userFolder, 0777, true)) {
                $file->move($userFolder, $file->getClientOriginalName());
                $this->setAvatarImage($userFolder.'/'.$file->getClientOriginalName());
                $this->setLinkAvatarImage(BASE_URL.$this->getAvatarImage());
                return array('error' => false, 'message' => 'User\'s avatar file was successful uploaded.');
            } else {
                return array('error' => true, 'message' => 'Error while creating user\'s folder.');
            }
        }
    }

    public function updateAvatarImageFile(UploadedFile $file) {
        $userFolder = USER_FOLDER.'user_id_'.$this->id;

        if ($this->avatarImage && $this->avatarImage != '' && $this->avatarImage != null) {
            $avatarImagePath = USER_FOLDER.'user_id_'.$this->id.'/'.$this->avatarImage;
            if (file_exists($avatarImagePath)) {
                unlink($avatarImagePath);

                $file->move($userFolder, $file->getClientOriginalName());
                $this->setAvatarImage($userFolder.'/'.$file->getClientOriginalName());
                $this->setLinkAvatarImage(BASE_URL.$this->getAvatarImage());
                return array('error' => false, 'message' => 'User\'s avatar file was successful uploaded.');
            } else {
                $this->saveAvatarImageFile($file);
            }
        } else {
            $this->saveAvatarImageFile($file);
        }
    }

    /**
     * Set linkAvatarImage
     *
     * @param string $linkAvatarImage
     *
     * @return User
     */
    public function setLinkAvatarImage($linkAvatarImage)
    {
        $this->linkAvatarImage = $linkAvatarImage;

        return $this;
    }

    /**
     * Get linkAvatarImage
     *
     * @return string
     */
    public function getLinkAvatarImage()
    {
        return $this->linkAvatarImage;
    }

    public function __construct() {
        parent::__construct();
    }
}

